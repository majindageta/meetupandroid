package com.davidericci.androidtest.viewmodel;

import android.arch.lifecycle.ViewModel;

import com.davidericci.androidtest.datamodel.User;
import com.davidericci.androidtest.network.DataProvider;

import java.util.List;

public class MainViewModel extends ViewModel {

    private List<User> userList;

    public List<User> getUserList(boolean forceReload) {
        if (forceReload || userList == null) {
            userList = loadUsers();
        }
        return userList;
    }

    private List<User> loadUsers() {
        return DataProvider.setupData().getList();
    }
}
