package com.davidericci.androidtest.network;

import com.davidericci.androidtest.AppContext;
import com.davidericci.androidtest.datamodel.User;

import java.util.ArrayList;
import java.util.Random;

public class DataProvider {

    private String men = "men";
    private String women = "women";
    private Random rand = new Random();
    private ArrayList<User> list = new ArrayList<>();
    static private DataProvider instance;

    public static DataProvider setupData() {
        if (instance == null) {
            instance = new DataProvider();
        }
        return instance;
    }

    private void getUsers() {
        list.clear();
        for (int i = 0; i < 4; i++) {
            String image;
            if ((rand.nextInt(2) + 1) == 1) {
                image = men + (rand.nextInt(10));
            } else {
                image = women + (rand.nextInt(10));
            }
            User user = new User();
            int resID = AppContext.getContext().getResources().getIdentifier(image , "drawable", AppContext.getContext().getPackageName());
            user.setImage(resID);
            list.add(user);
        }
    }

    public ArrayList<User> getList() {
        getUsers();
        return list;
    }
}
