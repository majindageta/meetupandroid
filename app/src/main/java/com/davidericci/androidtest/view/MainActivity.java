package com.davidericci.androidtest.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.davidericci.androidtest.R;
import com.davidericci.androidtest.datamodel.User;
import com.davidericci.androidtest.network.DataProvider;
import com.davidericci.androidtest.viewmodel.MainViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    MainViewModel usersViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        usersViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
//
//        showUsers(usersViewModel.getUserList(false));

        showUsers(DataProvider.setupData().getList());
    }

    //BUTTON ON CLICK
    public void showData(View v) {
        //FORCE NETWORK TO REDOWNLOAD DATA
        //SHOW NEW PICTURE
        showUsers(usersViewModel.getUserList(true));
    }

    private void showUsers(final List<User> users) {

        final ImageView imageView  = findViewById(R.id.imageView);
        final ImageView imageView2 = findViewById(R.id.imageView2);
        final ImageView imageView3 = findViewById(R.id.imageView3);
        final ImageView imageView4 = findViewById(R.id.imageView4);

        imageView.setImageDrawable(getDrawable(users.get(0).getImage()));
        imageView2.setImageDrawable(getDrawable(users.get(1).getImage()));
        imageView3.setImageDrawable(getDrawable(users.get(2).getImage()));
        imageView4.setImageDrawable(getDrawable(users.get(3).getImage()));
    }
}
