package com.davidericci.androidtest;

import android.content.Context;

public class AppContext extends android.app.Application {

    //REALLY SORRY FOR THIS BUT CONTEXT IS NEEDED TO LOAD IMAGE
    private static AppContext instance;

    public AppContext() {
        instance = this;
    }

    public static Context getContext() {
        return instance;
    }

}
